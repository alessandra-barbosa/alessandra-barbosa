
### Hi 👋 welcome to my GitHub :octocat:
#### About me:

- :octocat:  I'm Italian Brazilian statistician
- 🌱 I’m currently learning Data Science and Big Data
- 👯 I’m looking to collaborate on Data Science Projects


####  📫 How to reach me: 

[<img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" />](https://www.linkedin.com/in/alessandra-barbosa/) [<img src = "https://img.shields.io/badge/instagram-%23E4405F.svg?&style=for-the-badge&logo=instagram&logoColor=white">](https://www.instagram.com/alessandra-barbosa/) 
 [![Gmail Badge](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white&link=mailto:rsoliveira.c@gmail.com)](mailto:adeabarbosa@gmail.com)  [<img src="https://img.shields.io/badge/medium-%2312100E.svg?&style=for-the-badge&logo=medium&logoColor=white" />](https://medium.com/@alessandra-barbosa) 

<!--
**alessandra-barbosa/alessandra-barbosa** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.


-->
